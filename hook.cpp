// Copyright (C) 2014 Tomoyuki Fujimori <moyu@dromozoa.com>
//
// This file is part of dromozoa-package-hook.
//
// dromozoa-package-hook is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dromozoa-package-hook is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dromozoa-package-hook.  If not, see <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>

#include <dlfcn.h>
#include <errno.h>
#include <signal.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>

#include <string>
#include <vector>

extern char** environ;

////////////////////////////////////////////////////////////////////////////////

static std::vector<char> envp_buffer;
static std::vector<char*> envp_vector;
static char** envp;

void initialize_envp() {
  size_t size = 0;
  std::vector<std::string> buffer;
  for (char** ptr = environ; *ptr; ++ptr) {
    std::string v = *ptr;
    if (v.find("PATH=") == 0 || v.find("DROMOZOA_PACKAGE_") == 0) {
      size += v.size() + 1;
      buffer.push_back(v);
    }
  }

  envp_buffer.resize(size);
  envp_vector.clear();

  char* ptr = &envp_buffer[0];
  std::vector<std::string>::const_iterator i = buffer.begin();
  std::vector<std::string>::const_iterator end = buffer.end();
  for (; i != end; ++i) {
    const std::string& v = *i;
    memmove(ptr, v.c_str(), v.size() + 1);
    envp_vector.push_back(ptr);
    ptr += v.size() + 1;
  }

  envp_vector.push_back(0);
  envp = &envp_vector[0];
}


////////////////////////////////////////////////////////////////////////////////

static std::vector<char> command_buffer;
static char* command;

void initialize_command() {
  if (const char* const ptr = getenv("DROMOZOA_PACKAGE_HOOK_EXEC")) {
    const size_t size = strlen(ptr) + 1;
    command_buffer.resize(size);
    memmove(&command_buffer[0], ptr, size);
    command = &command_buffer[0];
  } else {
    command = 0;
  }
}

////////////////////////////////////////////////////////////////////////////////

#define DROMOZOA_PACKAGE_HOOK_EACH(name, result, arguments) \
  static result (*sys_##name) arguments; \
/**/
#include "each.hpp"

////////////////////////////////////////////////////////////////////////////////

inline void* async_signal_safe_memcpy(void* target, const void* source, size_t size) {
  char* t = static_cast<char*>(target);
  const char* s = static_cast<const char*>(source);
  for (size_t i = 0; i < size; ++i) {
    t[i] = s[i];
  }
  return target;
}

inline const char* async_signal_safe_strchr(const char* ptr, int c) {
  for (const char* i = ptr; *i != '\0'; ++i) {
    if (*i == c) {
      return i;
    }
  }
  return 0;
}

inline size_t async_signal_safe_strlen(const char* ptr) {
  size_t i = 0;
  for (; ptr[i] != '\0'; ++i) {}
  return i;
}

////////////////////////////////////////////////////////////////////////////////

inline size_t copy_string(const char* ptr, char* output, size_t size) {
  size_t length = async_signal_safe_strlen(ptr);
  if (length < size) {
    async_signal_safe_memcpy(output, ptr, length + 1);
    return length + 1;
  } else {
    return 0;
  }
}

inline size_t copy_int_length(int value) {
  size_t i = 0;
  if (value < 0) {
    value = -value;
    ++i;
  }
  do {
    value /= 10;
    ++i;
  } while (value > 0);
  return i;
}

inline size_t copy_int(int value, char* output, size_t size) {
  size_t length = copy_int_length(value);
  if (length < size) {
    if (value < 0) {
      output[0] = '-';
      value = -value;
    }
    size_t i = length - 1;
    do {
      output[i] = '0' + value % 10;
      value /= 10;
      --i;
    } while (value > 0);
    output[length] = '\0';
    return length + 1;
  } else {
    return 0;
  }
}

////////////////////////////////////////////////////////////////////////////////

class argument_vector {
public:
  argument_vector() : i_(), j_(), argv_() {}

  template <typename T>
  void push(const T* source) {
    if (source) {
      if (j_ < VECTOR_SIZE_ - 1) {
        if (size_t size = push_impl_(source)) {
          argv_ = &vector_[0];
          vector_[j_++] = &buffer_[i_];
          vector_[j_] = 0;
          i_ += size;
          return;
        }
      }
      argv_ = 0;
    }
  }

  char** get() const {
    return argv_;
  }

private:
  enum {
    BUFFER_SIZE_ = 1024,
    VECTOR_SIZE_ = 16,
  };

  char buffer_[BUFFER_SIZE_];
  char* vector_[VECTOR_SIZE_];
  size_t i_;
  size_t j_;
  char** argv_;

  size_t push_impl_(const char* source) {
    return copy_string(source, &buffer_[i_], BUFFER_SIZE_ - i_);
  }

  size_t push_impl_(const int* source) {
    return copy_int(*source, &buffer_[i_], BUFFER_SIZE_ - i_);
  }

  size_t push_impl_(const void*) {
    return 0;
  }
};

////////////////////////////////////////////////////////////////////////////////

inline bool is_successful(int result) {
  return result != -1;
}

template <typename T>
inline bool is_successful(T* result) {
  return result;
}

inline bool is_writer(int flag) {
  int mode = flag & O_ACCMODE;
  return mode == O_RDWR || mode == O_WRONLY;
}

inline bool is_writer(const char* mode) {
  return async_signal_safe_strchr(mode, 'w')
      || async_signal_safe_strchr(mode, 'a')
      || async_signal_safe_strchr(mode, '+');
}

inline void unset_close_on_execute(const int* source) {
  if (source) {
    int fd = *source;
    if (fd > -1) {
      int flag = fcntl(fd, F_GETFD);
      if (flag & FD_CLOEXEC) {
        flag &= ~FD_CLOEXEC;
        fcntl(fd, F_SETFD, flag);
      }
    }
  }
}

template <typename T>
inline void unset_close_on_execute(const T*) {}

////////////////////////////////////////////////////////////////////////////////

template <class T1, class T2, class T3, class T4, class T5>
inline void invoke_impl(const T1* arg1, const T2* arg2, const T3* arg3, const T4* arg4, const T5* arg5) {
  if (!command) {
    return;
  }

  argument_vector argv;
  argv.push(command);
  argv.push(arg1);
  argv.push(arg2);
  argv.push(arg3);
  argv.push(arg4);
  argv.push(arg5);
  if (!argv.get()) {
    return;
  }

  int save_errno = errno;
  sigset_t save_set;

  sigset_t set;
  sigemptyset(&set);
  sigaddset(&set, SIGCHLD);
  sigprocmask(SIG_BLOCK, &set, &save_set);

  pid_t pid = fork();
  if (pid == 0) {
    unset_close_on_execute(arg1);
    unset_close_on_execute(arg2);
    unset_close_on_execute(arg3);
    unset_close_on_execute(arg4);
    unset_close_on_execute(arg5);
    execve(command, argv.get(), envp);
    _exit(0);
  } else if (pid > 0) {
    waitpid(pid, 0, 0);
  }

  sigprocmask(SIG_SETMASK, &save_set, 0);
  errno = save_errno;
}

template <class T, class T1, class T2, class T3, class T4>
inline T invoke(
    const char* name, T result,
    const T1* arg1,
    const T2* arg2,
    const T3* arg3,
    const T4* arg4) {
  if (is_successful(result)) {
    invoke_impl(name, arg1, arg2, arg3, arg4);
  }
  return result;
}

template <class T, class T1, class T2, class T3>
inline T invoke(
    const char* name,
    T result,
    const T1* arg1,
    const T2* arg2,
    const T3* arg3) {
  return invoke(name, result, arg1, arg2, arg3, static_cast<const void*>(0));
}

template <class T, class T1, class T2>
inline T invoke(const char* name, T result, const T1* arg1, const T2* arg2) {
  return invoke(name, result, arg1, arg2, static_cast<const void*>(0));
}

template <class T, class T1>
inline T invoke(const char* name, T result, const T1* arg1) {
  return invoke(name, result, arg1, static_cast<const void*>(0));
}

////////////////////////////////////////////////////////////////////////////////

extern "C" {
  __attribute__((constructor)) void construct() {
    initialize_envp();
    initialize_command();
#define DROMOZOA_PACKAGE_HOOK_EACH(name, result, arguments) \
  sys_##name = reinterpret_cast<result (*) arguments>(dlsym(RTLD_NEXT, #name)); \
/**/
#include "each.hpp"
  }
}

////////////////////////////////////////////////////////////////////////////////

#define DROMOZOA_PACKAGE_HOOK_OPEN_MODE(mode, flag) \
  int mode = 0; \
  if (flag & O_CREAT) { \
    va_list ap; \
    va_start(ap, flag); \
    mode = va_arg(ap, int); \
    va_end(ap); \
  } \
/**/

extern "C" {
  int creat(const char* path, mode_t mode) {
    return invoke("creat", sys_creat(path, mode), path);
  }

  int open(const char* path, int flag, ...) {
    DROMOZOA_PACKAGE_HOOK_OPEN_MODE(mode, flag);
    if (is_writer(flag)) {
      return invoke("open", sys_open(path, flag, mode), path);
    } else {
      return sys_open(path, flag, mode);
    }
  }

#ifdef HAVE_OPENAT
  int openat(int fd, const char* path, int flag, ...) {
    DROMOZOA_PACKAGE_HOOK_OPEN_MODE(mode, flag);
    if (is_writer(flag)) {
      return invoke("openat", sys_openat(fd, path, flag, mode), &fd, path);
    } else {
      return sys_openat(fd, path, flag, mode);
    }
  }
#endif

#ifdef HAVE_CREAT64
  int creat64(const char* path, mode_t mode) {
    return invoke("creat64", sys_creat64(path, mode), path);
  }
#endif

#ifdef HAVE_OPEN64
  int open64(const char* path, int flag, ...) {
    DROMOZOA_PACKAGE_HOOK_OPEN_MODE(mode, flag);
    if (is_writer(flag)) {
      return invoke("open64", sys_open64(path, flag, mode), path);
    } else {
      return sys_open64(path, flag, mode);
    }
  }
#endif

#ifdef HAVE_OPENAT64
  int openat64(int fd, const char* path, int flag, ...) {
    DROMOZOA_PACKAGE_HOOK_OPEN_MODE(mode, flag);
    if (is_writer(flag)) {
      return invoke("openat64", sys_openat64(fd, path, flag, mode), &fd, path);
    } else {
      return sys_openat64(fd, path, flag, mode);
    }
  }
#endif
}

////////////////////////////////////////////////////////////////////////////////

extern "C" {
  int link(const char* path1, const char* path2) {
    return invoke("link", sys_link(path1, path2), path1, path2);
  }

  int rmdir(const char* path) {
    return invoke("rmdir", sys_rmdir(path), path);
  }

  int symlink(const char* path1, const char* path2) {
    return invoke("symlink", sys_symlink(path1, path2), path1, path2);
  }

  int truncate(const char* path, off_t length) {
    return invoke("truncate", sys_truncate(path, length), path);
  }

  int unlink(const char* path) {
    return invoke("unlink", sys_unlink(path), path);
  }

#ifdef HAVE_LINKAT
  int linkat(int fd1, const char* path1, int fd2, const char* path2, int flag) {
    return invoke("linkat", sys_linkat(fd1, path1, fd2, path2, flag), &fd1, path1, &fd2, path2);
  }
#endif

#ifdef HAVE_SYMLINKAT
  int symlinkat(const char* path1, int fd, const char* path2) {
    return invoke("symlinkat", sys_symlinkat(path1, fd, path2), path1, &fd, path2);
  }
#endif

#ifdef HAVE_UNLINKAT
  int unlinkat(int fd, const char* path, int flag) {
    return invoke("unlinkat", sys_unlinkat(fd, path, flag), &fd, path);
  }
#endif

#ifdef HAVE_TRUNCATE64
  int truncate64(const char* path, off64_t length) {
    return invoke("truncate64", sys_truncate64(path, length), path);
  }
#endif
}

////////////////////////////////////////////////////////////////////////////////

extern "C" {
  FILE* fopen(const char* path, const char* mode) {
    if (is_writer(mode)) {
      return invoke("fopen", sys_fopen(path, mode), path);
    } else {
      return sys_fopen(path, mode);
    }
  }

  FILE* freopen(const char* path, const char* mode, FILE* stream) {
    if (is_writer(mode)) {
      return invoke("freopen", sys_freopen(path, mode, stream), path);
    } else {
    return sys_freopen(path, mode, stream);
    }
  }

  int remove(const char* path) {
    return invoke("remove", sys_remove(path), path);
  }

  int rename(const char* path1, const char* path2) {
    return invoke("rename", sys_rename(path1, path2), path1, path2);
  }

#ifdef HAVE_RENAMEAT
  int renameat(int fd1, const char* path1, int fd2, const char* path2) {
    return invoke("renameat", sys_renameat(fd1, path1, fd2, path2), &fd1, path1, &fd2, path2);
  }
#endif

#ifdef HAVE_FOPEN64
  FILE* fopen64(const char* path, const char* mode) {
    if (is_writer(mode)) {
      return invoke("fopen64", sys_fopen64(path, mode), path);
    } else {
      return sys_fopen64(path, mode);
    }
  }
#endif

#ifdef HAVE_FREOPEN64
  FILE* freopen64(const char* path, const char* mode, FILE* stream) {
    if (is_writer(mode)) {
      return invoke("freopen64", sys_freopen64(path, mode, stream), path);
    } else {
    return sys_freopen64(path, mode, stream);
    }
  }
#endif
}

////////////////////////////////////////////////////////////////////////////////

extern "C" {
  int mkdir(const char* path, mode_t mode) {
    return invoke("mkdir", sys_mkdir(path, mode), path);
  }

  int mkfifo(const char* path, mode_t mode) {
    return invoke("mkfifo", sys_mkfifo(path, mode), path);
  }

  int mknod(const char* path, mode_t mode, dev_t dev) {
    return invoke("mknod", sys_mknod(path, mode, dev), path);
  }
}
