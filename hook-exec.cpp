// Copyright (C) 2014 Tomoyuki Fujimori <moyu@dromozoa.com>
//
// This file is part of dromozoa-package-hook.
//
// dromozoa-package-hook is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dromozoa-package-hook is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dromozoa-package-hook.  If not, see <http://www.gnu.org/licenses/>.

#include <fcntl.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/file.h>
#include <sys/time.h>
#include <syslog.h>
#include <time.h>
#include <unistd.h>

#include <algorithm>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

namespace dromozoa {
  inline std::string getcwd() {
    long path_max = pathconf(".", _PC_PATH_MAX);
    if (path_max == -1) {
      throw std::runtime_error("could not getpathconf");
    }
    std::vector<char> buffer(path_max);
    if (!::getcwd(&buffer[0], buffer.size())) {
      throw std::runtime_error("could not getcwd");
    }
    return &buffer[0];
  }

  inline std::string getenv(const char* key) {
    if (const char* ptr = ::getenv(key)) {
      return ptr;
    } else {
      throw std::runtime_error("could not getenv");
    }
  }

  inline std::string basename(const std::string& path) {
    std::vector<char> buffer(path.size() + 1);
    std::copy(path.begin(), path.end(), &buffer[0]);
    return ::basename(&buffer[0]);
  }

  inline std::string dirname(const std::string& path) {
    std::vector<char> buffer(path.size() + 1);
    std::copy(path.begin(), path.end(), &buffer[0]);
    return ::dirname(&buffer[0]);
  }

  class ios_flags_fill_saver {
  public:
    explicit ios_flags_fill_saver(std::ostream& out)
      : out_(out), flags_(out.flags()), fill_(out.fill()) {}

    ~ios_flags_fill_saver() {
      out_.flags(flags_);
      out_.fill(fill_);
    }

  private:
    std::ostream& out_;
    std::ios::fmtflags flags_;
    char fill_;

    ios_flags_fill_saver(const ios_flags_fill_saver&);
    ios_flags_fill_saver& operator=(const ios_flags_fill_saver&);
  };

  inline void quote_lua(const std::string& source, std::ostream& out) {
    ios_flags_fill_saver saver(out);
    out << std::hex << std::setfill('0') << "\"";
    std::string::const_iterator i = source.begin(), end = source.end();
    for (; i != end; ++i) {
      switch (char c = *i) {
        case '\0': out << "\\0";  break;
        case '\a': out << "\\a";  break;
        case '\b': out << "\\b";  break;
        case '\f': out << "\\f";  break;
        case '\n': out << "\\n";  break;
        case '\r': out << "\\r";  break;
        case '\t': out << "\\t";  break;
        case '\v': out << "\\v";  break;
        case '\\': out << "\\\\"; break;
        case '"':  out << "\\\""; break;
        case '\'': out << "\\'";  break;
        default:
          if (c < 0x20 || c == 0x7F) {
            out << "\\x" << std::setw(2) << static_cast<int>(c);
          } else {
            out << c;
          }
      }
    }
    out << "\"";
  }

  inline std::string now() {
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    gettimeofday(&tv, 0);

    time_t t = tv.tv_sec;

    struct tm tm;
    if (!gmtime_r(&t, &tm)) {
      throw std::runtime_error("could not gmtime_r");
    }

    std::ostringstream out;
    out << std::setfill('0')
        << std::setw(4) << (tm.tm_year + 1900)
        << "-"
        << std::setw(2) << (tm.tm_mon + 1)
        << "-"
        << std::setw(2) << tm.tm_mday
        << "T"
        << std::setw(2) << tm.tm_hour
        << ":"
        << std::setw(2) << tm.tm_min
        << ":"
        << std::setw(2) << tm.tm_sec
        << "."
        << std::setw(6) << tv.tv_usec
        << "Z";
    return out.str();
  }
}

inline void run(
    int save,
    int cwd,
    const std::string& name,
    const std::string& command,
    int fd,
    const std::string& path) {
  switch (fd) {
    case -1:
      break;
#ifdef AT_FDCWD
    case AT_FDCWD:
      break;
#endif
    default:
      if (fchdir(fd) == -1) {
        throw std::runtime_error("could not fchdir");
      }
  }

  if (chdir(dromozoa::dirname(path).c_str()) == -1) {
    throw std::runtime_error("could not chdir");
  }

  std::string pwd = dromozoa::getcwd();
  if (pwd != "/") {
    pwd += "/";
  }

  struct timeval tv;
  tv.tv_sec = 0;
  tv.tv_usec = 0;
  gettimeofday(&tv, 0);

  std::ostringstream out;
  out << "{name=";
  dromozoa::quote_lua(name, out);
  out << ",command=";
  dromozoa::quote_lua(command, out);
  out << ",path=";
  dromozoa::quote_lua(pwd + dromozoa::basename(path), out);
  out << ",now={sec=" << tv.tv_sec << ",usec=" << tv.tv_usec << "}};\n";

  std::string data = out.str();
  write(save, data.data(), data.size());

  if (fchdir(cwd) == -1) {
    throw std::runtime_error("could not fchdir");
  }
}

struct rule {
  const char* name;
  const char* command;
  int fd;
  int path;
};

static const rule rules[] = {
  { "openat",    "add", 2, 3 },
  { "openat64",  "add", 2, 3 },
  { "link",      "add", 0, 3 },
  { "rmdir",     "del", 0, 2 },
  { "symlink",   "add", 0, 3 },
  { "unlink",    "del", 0, 2 },
  { "linkat",    "add", 4, 5 },
  { "symlinkat", "add", 3, 4 },
  { "unlinkat",  "del", 2, 3 },
  { "remove",    "del", 0, 2 },
  { "rename",    "del", 0, 2 },
  { "rename",    "add", 0, 3 },
  { "renameat",  "del", 2, 3 },
  { "renameat",  "add", 4, 5 },
  { 0, 0, 0, 0 },
};

class scoped_fd {
public:
  explicit scoped_fd(int fd) : fd_(fd) {}

  ~scoped_fd() {
    if (fd_ != -1) {
      close(fd_);
    }
  }

  bool operator!() const {
    return fd_ == -1;
  }

  int get() const {
    return fd_;
  }

private:
  int fd_;

  scoped_fd(const scoped_fd&);
  scoped_fd& operator=(const scoped_fd&);
};

int main(int argc, char* argv[]) {
  try {
    if (argc < 2) {
      throw std::runtime_error("invalid argc");
    }
    std::string name = argv[1];

    scoped_fd lock(open(dromozoa::getenv("DROMOZOA_PACKAGE_HOOK_LOCK").c_str(), O_WRONLY | O_CREAT | O_APPEND, 0600));
    if (!lock) {
      throw std::runtime_error("could not open(DROMOZOA_PACKAGE_HOOK_LOCK)");
    }
    if (!lockf(lock.get(), F_LOCK, 0) == -1) {
      throw std::runtime_error("could not lockf");
    }

    scoped_fd save(open(dromozoa::getenv("DROMOZOA_PACKAGE_HOOK_SAVE").c_str(), O_WRONLY | O_CREAT | O_APPEND, 0600));
    if (!save) {
      throw std::runtime_error("could not open(DROMOZOA_PACKAGE_HOOK_SAVE)");
    }

    scoped_fd cwd(open(".", O_RDONLY));
    if (!cwd) {
      throw std::runtime_error("could not open");
    }

    bool found = false;
    for (int i = 0; rules[i].name; ++i) {
      const rule& r = rules[i];
      if (name != r.name) {
        continue;
      }

      int fd = -1;
      if (0 < r.fd) {
        if (r.fd < argc) {
          fd = atoi(argv[r.fd]);
        } else {
          throw std::runtime_error("invalid argc");
        }
      }

      const char* path = 0;
      if (r.path < argc) {
        path = argv[r.path];
      } else {
        throw std::runtime_error("invalid argc");
      }

      found = true;
      run(save.get(), cwd.get(), name, r.command, fd, path);
    }

    if (!found) {
      run(save.get(), cwd.get(), name, "add", -1, argv[2]);
    }

    return 0;
  } catch (const std::exception& e) {
    std::ostringstream out;
    out << e.what() << ":";
    for (int i = 0; i < argc; ++i) {
      out << " " << argv[i];
    }
    syslog(LOG_ERR, "%s", out.str().c_str());
    return 1;
  }
}
