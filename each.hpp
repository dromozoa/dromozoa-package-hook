// Copyright (C) 2014 Tomoyuki Fujimori <moyu@dromozoa.com>
//
// This file is part of dromozoa-package-hook.
//
// dromozoa-package-hook is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dromozoa-package-hook is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with dromozoa-package-hook.  If not, see <http://www.gnu.org/licenses/>.

// <fcntl.h>

DROMOZOA_PACKAGE_HOOK_EACH(creat, int, (const char*, mode_t))
DROMOZOA_PACKAGE_HOOK_EACH(open, int, (const char*, int, ...))

#ifdef HAVE_OPENAT
DROMOZOA_PACKAGE_HOOK_EACH(openat, int, (int, const char*, int, ...))
#endif

#ifdef HAVE_CREAT64
DROMOZOA_PACKAGE_HOOK_EACH(creat64, int, (const char*, mode_t))
#endif

#ifdef HAVE_OPEN64
DROMOZOA_PACKAGE_HOOK_EACH(open64, int, (const char*, int, ...))
#endif

#ifdef HAVE_OPENAT64
DROMOZOA_PACKAGE_HOOK_EACH(openat64, int, (int, const char*, int, ...))
#endif

// <unistd.h>

DROMOZOA_PACKAGE_HOOK_EACH(link, int, (const char*, const char*))
DROMOZOA_PACKAGE_HOOK_EACH(rmdir, int, (const char*))
DROMOZOA_PACKAGE_HOOK_EACH(symlink, int, (const char*, const char*))
DROMOZOA_PACKAGE_HOOK_EACH(truncate, int, (const char*, off_t))
DROMOZOA_PACKAGE_HOOK_EACH(unlink, int, (const char*))

#ifdef HAVE_LINKAT
DROMOZOA_PACKAGE_HOOK_EACH(linkat, int, (int, const char*, int, const char*, int))
#endif

#ifdef HAVE_SYMLINKAT
DROMOZOA_PACKAGE_HOOK_EACH(symlinkat, int, (const char*, int, const char*))
#endif

#ifdef HAVE_UNLINKAT
DROMOZOA_PACKAGE_HOOK_EACH(unlinkat, int, (int, const char*, int))
#endif

#ifdef HAVE_TRUNCATE64
DROMOZOA_PACKAGE_HOOK_EACH(truncate64, int, (const char*, off64_t))
#endif

// <stdio.h>

DROMOZOA_PACKAGE_HOOK_EACH(fopen, FILE*, (const char*, const char*))
DROMOZOA_PACKAGE_HOOK_EACH(freopen, FILE*, (const char*, const char*, FILE*))
DROMOZOA_PACKAGE_HOOK_EACH(remove, int, (const char*))
DROMOZOA_PACKAGE_HOOK_EACH(rename, int, (const char*, const char*))

#ifdef HAVE_RENAMEAT
DROMOZOA_PACKAGE_HOOK_EACH(renameat, int, (int, const char*, int, const char*))
#endif

#ifdef HAVE_FOPEN64
DROMOZOA_PACKAGE_HOOK_EACH(fopen64, FILE*, (const char*, const char*))
#endif

#ifdef HAVE_FREOPEN64
DROMOZOA_PACKAGE_HOOK_EACH(freopen64, FILE*, (const char*, const char*, FILE*))
#endif

// <sys/stat.h>

DROMOZOA_PACKAGE_HOOK_EACH(mkdir, int, (const char*, mode_t))
DROMOZOA_PACKAGE_HOOK_EACH(mkfifo, int, (const char*, mode_t))
DROMOZOA_PACKAGE_HOOK_EACH(mknod, int, (const char*, mode_t, dev_t))

#ifdef HAVE_MKDRIAT
DROMOZOA_PACKAGE_HOOK_EACH(mkdirat, int, (int, const char*, mode_t))
#endif

#ifdef HAVE_MKFIFOAT
DROMOZOA_PACKAGE_HOOK_EACH(mkfifoat, int, (int, const char*, mode_t))
#endif

#ifdef HAVE_MKNODAT
DROMOZOA_PACKAGE_HOOK_EACH(mknodat, int, (int, const char*, mode_t, dev_t))
#endif

#undef DROMOZOA_PACKAGE_HOOK_EACH
